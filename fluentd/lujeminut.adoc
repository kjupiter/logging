=== Hdfs

Checkout fluentd branch from:

	https://github.com/jarielpa/hadoop-docker.git
	

 Build:
 
	docker build -t hadoop-docker .
 
Run:

	docker run -it -e HOSTNAME=hadoop.10.2.2.2.nip.io --net=host -p 50070:50070 -p 50075:50075  -p 9000:9000 hadoop-docker /etc/bootstrap.sh -bash
	

or

	docker run -it -e HOSTNAME=hadoop.10.2.2.2.nip.io --net=host -p 50070:50070 -p 50075:50075  -p 9000:9000 hadoop-docker /etc/bootstrap.sh -d

Hostname should match config in output-hdfs.conf

=== Other Notes
S3 tested with minio and S3Proxy.
Do not work. WIP

==== S3Proxy
https://github.com/andrewgaul/s3proxy

s3proxy.conf:
	#s3proxy.authorization=none
	s3proxy.authorization=aws-v2
	s3proxy.identity=79P2VKEE2BS9H50QSN6H
	s3proxy.credential=1iH+QhQ968SnVXNrtnIAQ7leu5910UBrHSS8XKYa
	s3proxy.endpoint=http://10.2.2.2:9000
	jclouds.provider=filesystem
	jclouds.identity=identity
	jclouds.credential=credential
	jclouds.filesystem.basedir=/home/vagrant/logs

aws-v4 complains about missing method

aws-v2 answers but  something is still wrong

start:
	java -jar s3proxy.jar --properties=s3proxy.conf


=== Minio

https://github.com/arschles/minio-howto/blob/master/aggregating-Apache-logs-with-fluentd-and-Minio-server.md

start

 	./minio server ./pielog
 
 prints out access keys etc
 
 complains:
 
 class=RuntimeError error=#<RuntimeError: can't call S3 API. Please check your aws_key_id / aws_sec_key or s3_region configuration. error = #<Aws::S3::Errors::SignatureDoesNotMatch: The request signature we calculated does not match the signature you provided. Check your key and signing method