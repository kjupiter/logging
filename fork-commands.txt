# The nature of ElasticSearch requires tha a different scaling mechanism than the standard Kubernetes scaling is used.
# Deployer creates multiple deployments in order to scale ElasticSearch to multiple instances
# Indicate the desired scale at first deployment.

#uninstall new
oc delete oauthclient/kibana-proxy
oc delete configmap logging-deployer -n openshift
oc delete configmap logging-deployer 
oc new-app logging-deployer-template -p MODE=uninstall 
#install new
oc adm new-project logging --node-selector=""
oc project logging
oc apply -n openshift -f C:\projects\PIE\logging\hack\templates\dev-builds.yaml
oc apply -n openshift -f https://raw.githubusercontent.com/openshift/origin-aggregated-logging/master/deployer/deployer.yaml
oc new-app logging-dev-build-template -p FLUENTD_FORK_URL=https://kjupiter@bitbucket.org/kjupiter/logging.git,FLUENTD_FORK_BRANCH=master,LOGGING_FORK_BRANCH=release-1.3,LOGGING_FORK_URL=https://github.com/openshift/origin-aggregated-logging

oc new-app logging-deployer-account-template
oc adm policy add-cluster-role-to-user oauth-editor system:serviceaccount:logging:logging-deployer
oc adm policy add-scc-to-user privileged system:serviceaccount:logging:aggregated-logging-fluentd
oc adm policy add-cluster-role-to-user cluster-reader system:serviceaccount:logging:aggregated-logging-fluentd
oc create configmap logging-deployer --from-literal kibana-hostname=kibana.10.2.2.2.nip.io --from-literal kibana-hostname=kibana-ops.10.2.2.2.nip.io --from-literal public-master-url=https://10.2.2.2:8443 --from-literal es-instance-ram=512M from-literal es-cluster-size=3 --from-literal enable-ops-cluster=true --from-literal es-pvc-size=1G --from-literal es-pvc-prefix=logs-vol-claim
#oc create configmap logging-deployer --from-literal kibana-hostname=kibana.10.2.2.2.nip.io --from-literal kibana-hostname=kibana-ops.10.2.2.2.nip.io --from-literal public-master-url=https://10.2.2.2:8443 --from-literal es-instance-ram=512M from-literal es-cluster-size=3 --from-literal enable-ops-cluster=true --from-literal es-pvc-size=1G --from-literal es-pvc-prefix=logs-vol-claim -n openshift 
oc new-app logging-deployer-template  -p IMAGE_PREFIX=172.30.53.244:5000/logging/
oc label nodes --all logging-infra-fluentd=true --overwrite=true

# Step 2

cat <<EOF > /tmp/logging-ops-curator-config.yaml
.defaults:
  delete:
    days: 1
  runhour: 0
  runminute: 0
  timezone: Europe/Athens
EOF
oc delete configmap logging-curator-ops 
oc create configmap logging-curator-ops -o yaml --from-file=config.yaml=/tmp/logging-ops-curator-config.yaml | oc replace -f -
oc deploy --latest logging-curator-ops

cat <<EOF > /tmp/logging-curator-config.yaml
.defaults:
  delete:
    days: 1
  runhour: 0
  runminute: 0
  timezone: Europe/Athens
EOF
oc delete configmap logging-curator
oc create configmap logging-curator -o yaml --from-file=config.yaml=/tmp/logging-curator-config.yaml | oc replace -f -
oc deploy --latest logging-curator